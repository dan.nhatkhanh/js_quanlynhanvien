const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

let dsnv = [];

let dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  for (var index = 0; index < dsnv.length; index++) {
    let nv = dsnv[index];

    dsnv[index] = new NhanVien(
      nv.acc,
      nv.name,
      nv.email,
      nv.pass,
      nv.date,
      nv.salary,
      nv.position,
      nv.time
    );
  }
  renderDSNV(dsnv);
}

function themNV() {
  let newNV = getInfoFromForm();

  var isValid =
    validation.kiemTraTaiKhoan(
      newNV.acc,
      "tbTKNV",
      "Tài khoản nhân viên không được rỗng",
      "Tài khoản tối đa 4 - 6 ký số",
      4,
      6
    ) &
    validation.kiemTraNhanVien(
      newNV.name,
      "tbTen",
      "Tên nhân viên không được rỗng",
      "Tên nhân viên phải là chữ"
    ) &
    validation.kiemTraEmailNV(
      newNV.email,
      "tbEmail",
      "Email nhân viên không được rỗng",
      "Email nhân viên không hợp lệ"
    ) &
    validation.kiemTraMK(
      newNV.pass,
      "tbMatKhau",
      "Mật khẩu không được rỗng",
      6,
      10,
      "Mật khẩu từ 6-10 ký tự",
      "Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    ) &
    validation.kiemTraDate(
      newNV.date,
      "tbNgay",
      "Ngày làm không được rỗng",
      "Vui lòng viết theo định dạng: mm/dd/yyyy"
    ) &
    validation.kiemTraLuong(
      newNV.salary,
      "tbLuongCB",
      "Lương nhân viên không được rỗng",
      "Lương phải là số",
      1000000,
      20000000,
      "Lương cơ bản 1 000 000 - 20 000 000"
    ) &
    validation.kiemTraChucVu(
      newNV.position,
      "tbChucVu",
      "Vui lòng chọn chức vụ"
    ) &
    validation.kiemTraGioLam(
      newNV.time,
      "tbGiolam",
      "Giờ làm không được rỗng",
      "Giờ làm phải là số",
      80,
      200,
      "Số giờ làm trong tháng 80 - 200 giờ"
    );

  if (isValid) {
    dsnv.push(newNV);

    const uniqueIds = [];

    const unique = dsnv.filter((element) => {
      const isDuplicate = uniqueIds.includes(element.acc);

      if (!isDuplicate) {
        uniqueIds.push(element.acc);

        return true;
      }

      return false;
    });

    // tạo json
    var dsnvJson = JSON.stringify(unique);

    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(unique);
  }
  resetInfoInForm();
  document.getElementById(`btnDong`).click();
}

function xoaNhanVien(id) {
  console.log("id: ", id);

  var index = findAcc(id, dsnv);
  console.log(index);

  // tìm thấy vị trí
  if (index != -1) {
    dsnv.splice(index, 1);

    var dsnvJson = JSON.stringify(dsnv);

    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  document.getElementById(`btnThem`).click();

  var index = findAcc(id, dsnv);
  window["sua_id"] = id;
  console.log("index: ", index);
  if (index != -1) {
    var nv = dsnv[index];
    showInfoToForm(nv);
  }
}

function updateInfoNV() {
  const nvUpdate = getInfoFromForm();

  const id = nvUpdate.acc;

  var index = findAcc(id, dsnv);
  if (index != -1) {
    const dataFilter = dsnv.filter((f) => f.acc !== id);
    dataFilter.push(nvUpdate);
    console.log("dataFilter: ", dataFilter);

    //save localstorage

    const dataFilterJson = JSON.stringify(dataFilter);

    localStorage.setItem("DSNV_LOCALSTORAGE", dataFilterJson);

    renderDSNV(dataFilter);
  }

  location.reload();
}

function timLoaiNV(type) {
  let loaiinput = getTypeFromSearch(type);
  console.log("loaiinput: ", loaiinput);

  loaiinput = loaiinput.toLowerCase();

  let found = dsnv.filter((f) => f.type().includes(loaiinput));
  console.log("found: ", found);
  renderDSNV(found);
}
