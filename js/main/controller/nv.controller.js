function getInfoFromForm() {
  const acc = document.getElementById(`tknv`).value;
  const name = document.getElementById(`name`).value;
  const email = document.getElementById(`email`).value;
  const pass = document.getElementById(`password`).value;
  const date = document.getElementById(`datepicker`).value;
  const salary = document.getElementById(`luongCB`).value;
  const position =
    document.getElementById(`chucvu`).options[
      document.getElementById(`chucvu`).selectedIndex
    ].text;
  const time = document.getElementById(`gioLam`).value;

  let nv = new NhanVien(acc, name, email, pass, date, salary, position, time);

  console.log(nv.position);

  return nv;
}

function getTypeFromSearch(type) {
  return (type = document.getElementById(`searchName`).value);
}

function renderDSNV(nvArr) {
  let contentHTML = "";

  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];

    let trDanhSach = `
    <tr>
    <td>${nv.acc}</td>
    <td>${nv.name}</td>
    <td>${nv.email}</td>
    <td>${nv.date}</td>
    <td>${nv.position}</td>
    <td>${nv.finalSalary()}</td>
    <td>${nv.type()}</td>
    <td>
    <button onclick="xoaNhanVien('${nv.acc}')"
 class="btn btn-danger mb-2">Xóa</button>
    <button id="suanv" onclick="suaNhanVien('${nv.acc}')"
 class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;

    contentHTML += trDanhSach;
  }

  document.getElementById(`tableDanhSach`).innerHTML = contentHTML;
}

function findAcc(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.acc == id) {
      return index;
    }
  }

  return -1;
}

function showInfoToForm(nv) {
  document.getElementById(`tknv`).value = nv.acc;
  document.getElementById(`name`).value = nv.name;
  document.getElementById(`email`).value = nv.email;
  document.getElementById(`password`).value = nv.pass;
  document.getElementById(`datepicker`).value = nv.date;
  document.getElementById(`luongCB`).value = nv.salary;
  document.getElementById(`chucvu`).options[
    document.getElementById(`chucvu`).selectedIndex
  ].text = nv.position;
  document.getElementById(`gioLam`).value = nv.time;
  document.getElementById(`tknv`).readOnly = true;
}

function resetInfoInForm() {
  document.getElementById(`tknv`).value = "";
  document.getElementById(`name`).value = "";
  document.getElementById(`email`).value = "";
  document.getElementById(`password`).value = "";
  document.getElementById(`datepicker`).value = "";
  document.getElementById(`luongCB`).value = "";
  document.getElementById(`chucvu`).options[
    document.getElementById(`chucvu`).selectedIndex
  ].text = "";
  document.getElementById(`gioLam`).value = "";
}
