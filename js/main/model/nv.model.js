function NhanVien(acc, name, email, pass, date, salary, position, time) {
  this.acc = acc;
  this.name = name;
  this.email = email;
  this.pass = pass;
  this.date = date;
  this.salary = salary;
  this.position = position;
  this.time = time;
  this.finalSalary = function () {
    if (this.position == "Sếp") {
      return this.salary * 3;
    } else if (this.position == "Trưởng phòng") {
      return this.salary * 2;
    } else if (this.position == "Nhân viên") {
      return this.salary;
    }
  };
  this.type = function () {
    if (this.time >= 192) {
      return `nhân viên xuất sắc`;
    } else if (this.time >= 176) {
      return `nhân viên giỏi`;
    } else if (this.time >= 160) {
      return `nhân viên khá`;
    } else {
      return `nhân viên trung bình`;
    }
  };
}
