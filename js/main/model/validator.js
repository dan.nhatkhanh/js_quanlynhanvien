var validation = {
  kiemTraTaiKhoan: function (value, idError, message1, message2, min, max) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraNhanVien: function (value, idError, message1, message2) {
    const reLetter =
      /[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reLetter.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraEmailNV: function (value, idError, message1, message2) {
    const reEmail =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reEmail.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraMK: function (value, idError, message1, min, max, message2, message3) {
    const rePass =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else if (!rePass.test(value)) {
      document.getElementById(idError).innerText = message3;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraDate: function (value, idError, message1, message2) {
    const reDate = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/\d{4}$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reDate.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraLuong: function (
    value,
    idError,
    message1,
    message2,
    min,
    max,
    message3
  ) {
    const reNumber = /^[0-9]*$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reNumber.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else if (value < min || value > max) {
      document.getElementById(idError).innerText = message3;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraChucVu: function (text, idError, message) {
    if (text == "Chọn chức vụ") {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraGioLam: function (
    value,
    idError,
    message1,
    message2,
    min,
    max,
    message3
  ) {
    const reNumber = /^[0-9]*$/;

    if (value.length == 0) {
      document.getElementById(idError).innerText = message1;
      return false;
    } else if (!reNumber.test(value)) {
      document.getElementById(idError).innerText = message2;
      return false;
    } else if (value < min || value > max) {
      document.getElementById(idError).innerText = message3;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
};
